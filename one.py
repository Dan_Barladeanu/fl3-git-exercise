import time

def fact_for(num):
    sum = num
    for i in range(num-1,1,-1):
        sum = sum*i
    return sum

sum = 0
####
for i in range(100):
    start = time.time()
    fact = fact_for(50)
    end = time.time()
    sum = sum + end - start
    #print("time elapsed is: ", end - start)
avg = sum/100
print("Average time is: ", avg)

# Average run time for fact_for(50) is 2.5224685668945313e-06